# Microtick Proposal 1: Post-Genesis Validator Rewards

**The issue at hand:**

In the lead-up to Microtick’s genesis block, external validators (those not related to Microtick or ShapeShift) were incentivized at a rate of 20,000 TICK per validator. The resulting genesis distribution can be seen here: [https://microtick.com/mainnet-genesis.html](https://microtick.com/mainnet-genesis.html)

Left undefined, however, was how new validators would be incentivized. If Microtick is to meet its goal of achieving maximal consensus security, it’ll require a large and diverse validator set – ideally, something eventually approximating that of the Cosmos Hub.

With that in mind, Microtick (the entity) is proposing the following two-pronged policy that applies to the next 20 validators that join the network: \
 \
1.) New validators (who are not ShapeShift employees) should be incentivised at the rate of 5,000 TICK. These tokens would come from the remaining supply of 310,002 unallocated TICK. This TICK is held jointly by Microtick and ShapeShift in a multi-sig account. Current TICK supply is shown here: [https://microtick.com/chain-parameters.html](https://microtick.com/chain-parameters.html).

(Note that as with the pre-genesis validators, new validators will need to complete the ShapeShift KYC process.)

2.) In order to facilitate decentralization, ShapeShift employees serving as new validators should receive 1,000 TICK, the number of such validators not to exceed 10 under this proposal. (In other words, no more than 10 ShapeShift employees will be represented in the cohort of the next 20 overall validators).

This arguably strikes a good balance between incentivizing new validators, while doing so in a way that’s a.) sustainable, and b.) fair to the pre-genesis validators who took on a higher risk by supporting the zone prior to its launch.

Following the onboarding of 20 new validators, the community will need to decide what to do next, via the governance process. This will allow for adjustments and course corrections depending on future conditions.

**Arguments against:**

Clearly the zone must add validators over time in order to improve its security. However, there is potentially room for discussion around the amount of TICK to be distributed to each new validator.

Additionally, concerns have been raised that some validators could commit to supporting the network, and then simply disappear with their new tokens. This is certainly possible – although it’s worth noting that in those cases the “free riders” would need to delegate their TICK to existing validators (and therefore support the network’s security) in order to profit from their holdings.

**The formal proposal:**

The following will apply to the next 20 onboarded validators:

1.) New validators who are not ShapeShift employees will be incentivised at the rate of 5,000 TICK. 

2.) ShapeShift employees serving as new validators will receive 1,000 TICK, the number of such validators not to exceed 10 under this proposal.